package com.huike.report.mapper;


import com.huike.report.domain.vo.PieChartVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;


/**
 * 首页统计分析的Mapper
 * @author Administrator
 *
 */
public interface ReportMapper {
	/**=========================================基本数据========================================*/
	/**
	 * 获取线索数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getCluesNum(@Param("startTime") String beginCreateTime,
						@Param("endTime") String endCreateTime,
						@Param("username") String username);

	/**
	 * 获取商机数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getBusinessNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getContractNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同金额
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Double getSalesAmount(@Param("startTime") String beginCreateTime,
						  @Param("endTime") String endCreateTime,
						  @Param("username") String username);


	/**=================================================================================*/
	Integer getTodayCluesNum(@Param("username") String username, @Param("today") String today);

    Integer getTodayBusinessNum(@Param("username") String username, @Param("today") String today);

	Integer getTodayContractNum(@Param("username") String username, @Param("today") String today);

	Double getTodaySalesAmount(@Param("username") String username, @Param("today") String today);

    Integer getTofollowedCluesNum(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime, @Param("username") String username);

	Integer getTofollowedBusinessNum(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime, @Param("username") String username);

	Integer getToallocatedCluesNum(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime, @Param("username") String username);

	Integer getToallocatedBusinessNum(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime, @Param("username") String username);

    List<Map<String, Object>> subjectStatistics(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);



}